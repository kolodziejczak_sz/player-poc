import { FunctionComponent, ComponentClass } from 'react';
import Channel from './Channel/Channel';
import NotFound from './NotFound';

export type RouteComponent = FunctionComponent | ComponentClass;

export type Route = {
    component: RouteComponent;
    name: string;
    path: string;
    exact?: boolean;
    [key: string]: any;
};

export const routes: Route[] = [
    { path: '/', component: Channel, name: 'channel', exact: true },
    { path: '*', component: NotFound, name: 'notfound' },
];

export default routes;
