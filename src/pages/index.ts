import Home from './Channel/Channel';
import NotFound from './NotFound';
import routes, { Route as CustomRoute } from './routes';

export type Route = CustomRoute;

export { Home, NotFound, routes };
