import Observable from '../../lib/observable';
import { FOO_PLAYER_EVENTS } from './../../lib/fooPlayer';

const PLAYER_USER_EVENTS = {
    PLAY_INTENT: 'play_intent',
    PAUSE_INTENT: 'pause_intent',
    RESUME_INTENT: 'resume_intent',
};

export const PLAYER_EVENTS = Object.assign(
    {},
    FOO_PLAYER_EVENTS,
    PLAYER_USER_EVENTS,
);

export interface PlayerEvent {
    type: string;
    data?: any;
}

export const PlayerEventsBus = Observable<PlayerEvent>();

export { PLAYER_USER_EVENTS, FOO_PLAYER_EVENTS };
