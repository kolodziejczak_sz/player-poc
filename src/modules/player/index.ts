import reducer from './reducer';
import { PlayerEvent as IPlayerEvent } from './EventsBus';

export {
    PlayerEventsBus,
    PLAYER_EVENTS,
    FOO_PLAYER_EVENTS,
    PLAYER_USER_EVENTS,
} from './EventsBus';

export { playMovie, pause, resume, error } from './actions';

export interface PlayerEvent extends IPlayerEvent {}

export default reducer;
