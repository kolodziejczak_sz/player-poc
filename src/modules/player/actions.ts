import { Dispatch } from 'redux';
import { PlayerEventsBus, PLAYER_EVENTS } from './EventsBus';

export type PlayerActionsTypes = 'PLAY' | 'PAUSE' | 'RESUME' | 'STOP' | 'ERROR';

export interface PlayerAction {
    type: PlayerActionsTypes;
    payload?: string;
}

export const playMovie = () => (dispatch: Dispatch, getState: Function) => {
    const canPlay = Boolean(getState());
    const movieId = '1';

    if (canPlay) {
        dispatch({
            type: 'PLAY',
            payload: movieId,
        });
        PlayerEventsBus.next({
            type: PLAYER_EVENTS.PLAY_INTENT,
            data: movieId,
        });
    } else {
        dispatch({
            type: 'ERROR',
            payload: 'err',
        });
    }
};
export const pause = () => (dispatch: Dispatch) => {
    PlayerEventsBus.next({ type: PLAYER_EVENTS.PAUSE_INTENT });
    dispatch({ type: 'PAUSE' });
};

export const error = (err: string) => ({ type: 'ERROR', payload: err });

export const resume = () => (dispatch: Dispatch) => {
    PlayerEventsBus.next({ type: PLAYER_EVENTS.RESUME_INTENT });
    dispatch({ type: 'RESUME' });
};
