import { PlayerAction } from './actions';

type PlayerState = {
    error: string;
    movieId: string;
};

const initialState: PlayerState = {
    error: '',
    movieId: '',
};

const reducer = (state: PlayerState = initialState, action: PlayerAction) => {
    switch (action.type) {
        case 'PLAY': {
            return {
                ...state,
                movieId: action.payload || '',
            };
        }
        case 'ERROR': {
            return {
                ...state,
                error: action.payload || '',
            };
        }
        case 'RESUME':
        case 'PAUSE':
        default: {
            return state;
        }
    }
};

export default reducer;
