import { applyMiddleware, compose, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import player from './modules/player';

const composeEnhancers =
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    player,
});

const enhancers = composeEnhancers(applyMiddleware(thunk));

export default createStore(rootReducer, enhancers);
