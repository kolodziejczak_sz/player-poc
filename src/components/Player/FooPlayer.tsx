import { PureComponent } from 'react';
import { fooPlayer, FooPlayerApi } from '../../lib/fooPlayer';
import {
    FOO_PLAYER_EVENTS,
    PLAYER_USER_EVENTS,
    PlayerEventsBus,
    PlayerEvent,
} from '../../modules/player';

class FooPlayer extends PureComponent {
    private player: FooPlayerApi;
    private onUnmount: Array<Function> = [];

    constructor(props: any) {
        super(props);
        // Initialize Foo Player.
        this.player = fooPlayer;

        // Connect Foo Player events to EventsBus
        Object.values(FOO_PLAYER_EVENTS).forEach((eventName: string) => {
            this.onUnmount.push(
                this.player.on(eventName, (data: any) =>
                    PlayerEventsBus.next({
                        type: eventName,
                        data,
                    }),
                ),
            );
        });

        // Listen to EventsBus
        this.onUnmount.push(PlayerEventsBus.subscribe(this.onEvent));
    }

    onEvent = ({ type: intent }: PlayerEvent) => {
        const player = this.player;

        switch (intent) {
            case PLAYER_USER_EVENTS.PLAY_INTENT:
                player.play();
                break;
            case PLAYER_USER_EVENTS.PAUSE_INTENT:
                player.pause();
                break;
            case PLAYER_USER_EVENTS.RESUME_INTENT:
                player.resume();
                break;
            default:
                break;
        }
    };

    componentWillUnmount() {
        this.onUnmount.forEach((todo: Function) => todo());
    }

    render() {
        return null;
    }
}

export default FooPlayer;
