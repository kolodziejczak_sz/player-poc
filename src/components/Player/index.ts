import Player from './Player';
import PlayerSlot from './PlayerSlot';
import withPlayerEventsBus from './withPlayerEventsBus';

export { Player, PlayerSlot, withPlayerEventsBus };

export default Player;
