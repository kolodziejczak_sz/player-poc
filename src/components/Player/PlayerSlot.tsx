import { useEffect, useRef, ReactNode } from 'react';
import ReactDOM from 'react-dom';

interface PlayerSlotProps {
    children: ReactNode;
}

const PlayerSlot = ({ children }: PlayerSlotProps) => {
    const { current: el } = useRef(document.createElement('div'));

    useEffect(() => {
        const anchor = document.querySelector('[js-player-slots-container]');

        if (anchor !== null) {
            anchor.appendChild(el);

            return () => {
                anchor.removeChild(el);
            };
        }
    }, [el]);

    return ReactDOM.createPortal(children, el);
};

export default PlayerSlot;
