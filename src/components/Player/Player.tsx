import React from 'react';
import cx from 'classnames';
import FooPlayer from './FooPlayer';
import PlayerError from '../PlayerError';
import './Player.scss';

interface PlayerProps {
    className?: string;
    Toolkit: Function;
}

const Player = ({ className, Toolkit }: PlayerProps) => (
    <div className={cx(className, 'player')}>
        <div className="player__layout" js-player-slots-container="true" />
        <PlayerError />
        <Toolkit />
        <FooPlayer />
    </div>
);

export default Player;
