import { withProps } from 'recompose';
import { omit } from 'lodash';
import { PlayerEventsBus } from '../../modules/player';

const eventsBusWithoutNext = omit(PlayerEventsBus, 'next');

export default withProps({ playerEventsBus: eventsBusWithoutNext });
