import React, { useState, useEffect } from 'react';
import cx from 'classnames';
import { PlayerEvent, PLAYER_EVENTS } from '../modules/player';
import { Observable } from '../lib/observable';
import { withPlayerEventsBus } from './Player';

interface CurrentTimeProps {
    className?: string;
    playerEventsBus: Observable<PlayerEvent>;
}

const CurrentTime = ({ className, playerEventsBus }: CurrentTimeProps) => {
    const [currentTime, setCurrentTime] = useState(0);

    useEffect(
        () =>
            playerEventsBus.subscribe(({ type, data }) => {
                if (type === PLAYER_EVENTS.TIME_CHANGED) {
                    setCurrentTime(data);
                }
            }),
        [playerEventsBus],
    );

    return (
        <span className={cx(className)}>
            <div>{currentTime}</div>
        </span>
    );
};

export default withPlayerEventsBus(CurrentTime);
