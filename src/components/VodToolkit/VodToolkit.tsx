import React from 'react';
import PlayButton from '../PlayButton';
import Time from '../CurrentTime';
import { PlayerSlot } from '../Player';
import './VodToolkit.scss';

const VodToolkit = () => (
    <PlayerSlot>
        <PlayButton className="vod__play" />
        <Time className="vod__time" />
    </PlayerSlot>
);

export default VodToolkit;
