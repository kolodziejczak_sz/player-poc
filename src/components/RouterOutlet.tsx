import React from 'react';
import { Route as ReactRouterRoute, Switch } from 'react-router-dom';
import { routes, Route } from '../pages';

const RouterOutlet = () => (
    <Switch>
        {routes.map(({ name, ...rest }: Route) => (
            <ReactRouterRoute key={name} {...rest}></ReactRouterRoute>
        ))}
    </Switch>
);

export default RouterOutlet;
