import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import { PlayerEvent, PLAYER_EVENTS, error } from '../modules/player';
import { Observable } from '../lib/observable';
import { withPlayerEventsBus } from './Player';

interface PlayerErrorProps {
    className?: string;
    playerEventsBus: Observable<PlayerEvent>;
    msg: string;
}

const PlayerError = ({ className, playerEventsBus, msg }: PlayerErrorProps) => {
    useEffect(
        () =>
            playerEventsBus.subscribe(({ type, data }) => {
                if (type === PLAYER_EVENTS.ERROR) {
                    error(data);
                }
            }),
        [playerEventsBus],
    );

    return (
        <span className={cx(className)}>
            <div>{msg}</div>
        </span>
    );
};

const mapStateToProps = (state: any) => ({
    msg: state.player.error,
});

const mapDispatchToProps = {
    error,
};

export default withPlayerEventsBus(
    connect(mapStateToProps, mapDispatchToProps)(PlayerError),
);
