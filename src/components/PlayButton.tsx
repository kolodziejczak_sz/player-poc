import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import {
    PlayerEvent,
    PLAYER_EVENTS,
    playMovie,
    pause,
    resume,
} from '../modules/player';
import { Observable } from '../lib/observable';
import { withPlayerEventsBus } from './Player';

interface PlayButtonProps {
    className?: string;
    movieId: string;
    playMovie: Function;
    pause: Function;
    resume: Function;
    playerEventsBus: Observable<PlayerEvent>;
}

const PlayButton = ({
    className,
    playerEventsBus,
    movieId,
    pause,
    resume,
    playMovie,
}: PlayButtonProps) => {
    const [currentPlayButton, setCurrentPlayButton] = useState(0);

    const onClick = () => {
        if (movieId) {
            currentPlayButton ? pause() : resume();
        } else {
            playMovie();
        }
    };

    useEffect(
        () =>
            playerEventsBus.subscribe(({ type, data }) => {
                if (type === PLAYER_EVENTS.PLAY_STATE_CHANGED) {
                    setCurrentPlayButton(data);
                }
            }),
        [playerEventsBus],
    );

    return (
        <button onClick={onClick} className={cx(className)}>
            {currentPlayButton ? 'PAUSE' : 'PLAY'}
        </button>
    );
};

const mapStateToProps = (state: any) => ({
    movieId: state.player.movieId,
});

const mapDispatchToProps = {
    playMovie,
    pause,
    resume,
};

export default withPlayerEventsBus(
    connect(mapStateToProps, mapDispatchToProps)(PlayButton),
);
