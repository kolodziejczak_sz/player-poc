import React from 'react';
import RouterOutlet from '../components/RouterOutlet';
import './App.scss';

const App: React.FC = () => {
    return (
        <div className="App">
            <RouterOutlet />
        </div>
    );
};

export default App;
