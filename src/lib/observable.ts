type UnsubscribeFunction = () => void;
type SubscribeHandler<T> = (value: T) => any;
export interface Observable<T> {
    next: (value: T) => void;
    subscribe: (handler: SubscribeHandler<T>) => UnsubscribeFunction;
}

function Observable<T>() {
    const subscribers = new Set<SubscribeHandler<T>>();

    const next = (value: T): void => {
        subscribers.forEach((h: SubscribeHandler<T>) => {
            h(value);
        });
    };

    const subscribe = (handler: SubscribeHandler<T>): UnsubscribeFunction => {
        subscribers.add(handler);

        return () => {
            subscribers.delete(handler);
        };
    };

    return { subscribe, next } as Observable<T>;
}

export default Observable;
