import { zipObject } from 'lodash';
import xs, { Subscription } from 'xstream';

export const FOO_PLAYER_EVENTS = {
    PLAY: 'foo_play',
    RESUME: 'foo_resume',
    PAUSE: 'foo_pause',
    ERROR: 'foo_error',
    PLAY_STATE_CHANGED: 'foo_play_state_changed',
    TIME_CHANGED: 'foo_time_changed',
    END: 'foo_end',
};

export interface FooPlayerApi {
    play: () => void;
    pause: () => void;
    resume: () => void;
    on: (eventName: string, handler: Function) => Function;
    isPlaying: () => boolean;
    getCurrentTime: () => number;
}

type EventsSubscribers = {
    [eventName: string]: Array<Function>;
};

const subsPerEvent = ((): EventsSubscribers => {
    const eventNames = Object.values(FOO_PLAYER_EVENTS);
    const emptyArrays = eventNames.map(_ => []);
    const subsPerEvent = zipObject(eventNames, emptyArrays);

    return subsPerEvent as EventsSubscribers;
})();

const trigger = (eventName: string, args?: any): void => {
    subsPerEvent[eventName].forEach((handler: Function) => {
        handler(args);
    });
};

const on = (eventName: string, handler: Function): Function => {
    subsPerEvent[eventName].push(handler);

    return () => {
        subsPerEvent[eventName].filter((h: Function) => h !== handler);
    };
};

let duration = 10;
let currentTime = 0;
let playing = false;
let playStreamSubscription: Subscription;

const play = (): void => {
    console.log('play');

    if (playStreamSubscription) {
        playStreamSubscription.unsubscribe();
    }

    trigger(FOO_PLAYER_EVENTS.PLAY);
    trigger(FOO_PLAYER_EVENTS.PLAY_STATE_CHANGED, true);

    currentTime = 0;
    playing = true;
    playStreamSubscription = xs
        .periodic(1_000)
        .filter(_ => playing)
        .take(duration)
        .subscribe({
            next: (_: any) => {
                currentTime += 1;
                console.log('currentTime', currentTime);
                trigger(FOO_PLAYER_EVENTS.TIME_CHANGED, currentTime);
            },
            error: (_: any) => {
                console.log('error');
                trigger(FOO_PLAYER_EVENTS.ERROR);
            },
            complete: () => {
                console.log('end');
                trigger(FOO_PLAYER_EVENTS.END);
                trigger(FOO_PLAYER_EVENTS.PLAY_STATE_CHANGED, false);
            },
        });
};

const pause = (): void => {
    console.log('pause');
    playing = false;
    trigger(FOO_PLAYER_EVENTS.PAUSE);
    trigger(FOO_PLAYER_EVENTS.PLAY_STATE_CHANGED, false);
};

const resume = (): void => {
    console.log('resume');
    playing = true;
    trigger(FOO_PLAYER_EVENTS.RESUME);
    trigger(FOO_PLAYER_EVENTS.PLAY_STATE_CHANGED, true);
};

const isPlaying = (): boolean => {
    return playing;
};

const getCurrentTime = (): number => {
    return currentTime;
};

(window as any)['__play'] = play;
(window as any)['__pause'] = pause;
(window as any)['__resume'] = resume;
(window as any)['__isPlaying'] = isPlaying;
(window as any)['__getCurrentTime'] = getCurrentTime;

export const fooPlayer: FooPlayerApi = {
    play,
    pause,
    resume,
    on,
    isPlaying,
    getCurrentTime,
};
